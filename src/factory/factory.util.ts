import { FactoryFn } from './factory.interfaces';



export class FactoryUtil {



  static produce<PRODUCT>(factory: FactoryFn<PRODUCT>): PRODUCT {
    const product = factory();

    return product;
  }
  
}
