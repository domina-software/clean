


export interface FactoryFn<PRODUCT> {
  (): PRODUCT
}



export interface FactoryOf<T> {
  (): T;
}
