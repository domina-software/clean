import { FactoryUtil } from './factory.util';



describe('FactoryUtil', () => {
  


  describe('produce', () => {
    


    it('should call the passed factoryFn', () => {
      const fn = jest.fn();

      FactoryUtil.produce(fn);

      expect(fn).toHaveBeenCalled();
    });
    


    it('should return the result of factoryFn', () => {
      const value = '🌞';
      const fn = jest.fn().mockReturnValue(value);

      const result = FactoryUtil.produce(fn);

      expect(result).toBe(value);
    });
    
  });
  
});
