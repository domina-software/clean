import { SystemToolSet, SystemToolSetWithFactories } from './systemTool.interfaces';
import { FactoryOf } from '../factory';
import { ObjectUtil } from '../util/object.util';



export class SystemToolUtil {



  static combineToolSets(...toolSets: (Partial<SystemToolSetWithFactories> | undefined)[]): SystemToolSetWithFactories {
    const combinedToolSets = toolSets
                              .filter(t => !!t)
                              .reduce<Partial<SystemToolSetWithFactories>>((acc, toolSet) => {
                                return { ...acc, ...toolSet };
                              }, {});

    const missingTools = SystemToolUtil.missingTools(combinedToolSets);
    if (missingTools.length > 0) throw Error(SystemToolUtil.ErrorMsg.combineToolSets.missingTools(missingTools));

    return combinedToolSets as SystemToolSetWithFactories;
  }



  static missingTools(
    toolSet: Partial<SystemToolSetWithFactories>,
    mandatoryTools = SystemToolUtil.mandatoryTools
  ): string[] {
    
    const missing = mandatoryTools.filter(t => !toolSet.hasOwnProperty(t));

    return missing;
  }
  static mandatoryTools = [ 'console' ];



  static filterToolFactories<TOOL_SET extends Partial<SystemToolSetWithFactories>>(toolSet: TOOL_SET): { [name: string]: FactoryOf<any> } {
    const toolFactories: { [key: string ]: any } = {};
    for (let toolName in toolSet) {
      if (toolSet.hasOwnProperty(toolName)) {
        // @ts-ignore
        const value = toolSet[toolName];

        if (typeof value === 'function') toolFactories[toolName] = value;
      }
    }

    return toolFactories;
  }



  static productToolFromFactories(toolSet: Partial<SystemToolSetWithFactories>): Partial<SystemToolSet> {
    const onlyToolFactories = SystemToolUtil.filterToolFactories(toolSet);

    const entries = Object.entries(onlyToolFactories);
    const producedToolsEntries: [string, FactoryOf<any>][] = entries.map(([name, factory]) => [name, SystemToolUtil.produceToolFromFactory(factory)]);

    const producedTools: Partial<SystemToolSet> = ObjectUtil.fromEntries(producedToolsEntries);

    return producedTools;
  }



  static produceToolFromFactory<TOOL>(toolFactory: FactoryOf<TOOL>): TOOL {
    const tool = toolFactory();

    return tool;
  }



  static ErrorMsg = {
    combineToolSets: {
      missingTools: (toolNames: string[]) => `These mandatory tools are missing: ${ JSON.stringify(toolNames) }`
    }
  };
  
}
