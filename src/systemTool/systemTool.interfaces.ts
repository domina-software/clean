import { Console } from '../console';
import { FactoryOf } from '../factory/factory.interfaces';



export interface SystemToolSetWithFactories {
  console: Console | FactoryOf<Console>;
}



export interface SystemToolSet {
  console: Console;
}
