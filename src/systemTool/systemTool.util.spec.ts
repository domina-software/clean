import { SystemToolUtil } from './systemTool.util';
import { SystemToolSet } from './systemTool.interfaces';
import { Console } from '../console';



describe('SystemToolUtil', () => {
  


  describe('combineToolSets', () => {
    


    it('should throw an error if there is some missing tools', () => {
      const missingTools = [ 'missing tool 1', 'missing tool 2' ];
      spyOn(SystemToolUtil, 'missingTools').and.returnValue(missingTools);

      const toolSet: SystemToolSet = { console: null as unknown as Console };

      const expectedError = SystemToolUtil.ErrorMsg.combineToolSets.missingTools(missingTools);

      expect(() => {
        SystemToolUtil.combineToolSets(toolSet);
      }).toThrowError(expectedError);
    });
    


    it('should combine toolSets', () => {
      const toolSet_1 = { console: null as unknown as Console, a: 1            };
      const toolSet_2 = { console: null as unknown as Console,      b: 2       };
      const toolSet_3 = { console: null as unknown as Console,      b: 4, c: 3 };

      const expected_combination = { console: null as unknown as Console, a: 1, b: 4, c: 3 };

      const result = SystemToolUtil.combineToolSets(toolSet_1, toolSet_2, toolSet_3);

      expect(result).toEqual(expected_combination);
    });
    
  });



  describe('missingTools', () => {
    


    it('should return an empty array every tool is present', () => {
      const toolSet = { console: null, a: 1 } as unknown as SystemToolSet;
      const mandatory = [ 'console', 'a' ];

      const result = SystemToolUtil.missingTools(toolSet, mandatory);

      expect(result).toEqual([]);
    });
    


    it('should return an array with missing tool names', () => {
      const toolSet = { console: null, a: 1 } as unknown as SystemToolSet;
      const missing = [ 'b', 'c' ];
      const mandatory = [ 'console', 'a', ...missing ];

      const result = SystemToolUtil.missingTools(toolSet, mandatory);

      expect(result).toEqual(missing);
    });
    
  });



  describe('filterToolFactories', () => {
    


    it('should return an empty object if no factory is present', () => {
      const toolSet = { console: null, a: 1 } as unknown as SystemToolSet;

      const result = SystemToolUtil.filterToolFactories(toolSet);

      expect(result).toEqual({});
    });

    it('should return an object with factories', () => {
      const fn_1 = () => 1;
      const fn_2 = () => 2;

      const toolSet = { console: null, fn_1, a: 1, fn_2 } as unknown as SystemToolSet;

      const result = SystemToolUtil.filterToolFactories(toolSet);

      expect(result).toEqual({ fn_1, fn_2 });
    });
    
  });



  describe('produceToolFromFactory', () => {
    


    it('should call the passed toolFactory and return its result', () => {
      const toBeReturned = 42;
      const fn = jest.fn().mockReturnValue(toBeReturned);

      const result = SystemToolUtil.produceToolFromFactory(fn);

      expect(fn).toHaveBeenCalled();
      expect(result).toBe(toBeReturned);
    });
    
  });



  describe('ErrorMsg', () => {
    


    describe('combineToolSets', () => {
      


      describe('missingTools', () => {
        


        it('should return a string containing missing tool names', () => {
          const missing = [ 'a', 'b' ];
          const result = SystemToolUtil.ErrorMsg.combineToolSets.missingTools(missing);

          missing.forEach(m => expect(result).toContain(m));
        });
        
      });
      
    });
    
  });
  
});
