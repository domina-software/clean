import { Delegatee } from '..';



export class DelegateeUtil {



  static isValid(toBeValidated: Delegatee): boolean {
    try {
      const has_getDelegation = toBeValidated.hasOwnProperty('getDelegation');
      
      return has_getDelegation;

    } catch (error) {
      return false;
    }
  }



  static filterValidOnes(toBeFiltered: Delegatee[]): Delegatee[] {
    try {
      if (!Array.isArray(toBeFiltered)) throw Error();

      const filteredDelegatees = toBeFiltered.filter(DelegateeUtil.isValid);
      
      return filteredDelegatees;

    } catch (error) {
      return [];
    }
  }



  static placeHolder: Delegatee = {
    getDelegation: () => true
  }
  
}
