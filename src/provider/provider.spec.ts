import { Provider } from './provider';
import { Providable, ProvidableWithInit } from '../providable';
import { Criteria } from '../criteria/criteria';
import { ProvidableUtil } from '../providable/providable.util';
import { NameUtil } from '../forHuman/nameUtil/name.util';



describe('provider', () => {
  const valid_providable_01: Providable = { name: 'providable 01', desc:  'first valid sample of a providable' };
  const valid_providable_02: Providable = { name: 'providable 02', desc: 'second valid sample of a providable' };
  const valid_providable_03: Providable = { name: 'providable 03', desc:  'third valid sample of a providable' };
  
  const valid_providable_withInit_01: ProvidableWithInit = { name: 'providable with init 01', desc:  'first valid sample of a providable with init', init: async () => valid_providable_withInit_01 };
  const valid_providable_withInit_02: ProvidableWithInit = { name: 'providable with init 02', desc: 'second valid sample of a providable with init', init: async () => valid_providable_withInit_02 };
  const valid_providable_withInit_03: ProvidableWithInit = { name: 'providable with init 03', desc:  'third valid sample of a providable with init', init: async () => valid_providable_withInit_03 };
  

  const criteria_always_true = new Criteria(
    (providable: Providable) => true,
    'always true',
    'this criteria always return true'
  );

  const criteria_always_false = new Criteria(
    () => false,
    'always false',
    'this criteria always return false'
  );
  
  
  describe('constructor', () => {
    const providables = [
      valid_providable_01,
      valid_providable_02,
      valid_providable_03
    ];
    


    it('should assign the providables attribute', () => {
      const provider = new Provider(providables);

      expect(provider['providables']).toBe(providables);
    });

  });



  describe('provide One/Multiple', () => {
    let providables: Providable[];
    let provider: Provider<Providable>;

    beforeAll(() => {
      providables = [
        valid_providable_01,
        valid_providable_02,
        valid_providable_03
      ];

      provider = new Provider(providables);
    });



    it('should return only 1 (the first) providable even if tilterMetCriteria returns more', async () => {
      // @ts-ignore
      spyOn(provider, 'provide').and.returnValue(providables);

      const result = await provider.provideOne(criteria_always_true);

      expect(result).toBe(providables[0]);
    });



    describe('provideByName', () => {
      


      it('should call createCriteriaToProvideByName passing the providable\'s name', () => {
        const providableName = 'name';

        // @ts-ignore
        const spy = spyOn(provider, 'createCriteriaToProvideByName').and.returnValue(criteria_always_true);

        provider.provideByName(providableName);

        expect(spy).toHaveBeenCalledWith(providableName);
      });



      it('should pass createCriteriaToProvideByName\'s result to provideOne', () => {
        // @ts-ignore
        spyOn(provider, 'createCriteriaToProvideByName').and.returnValue(criteria_always_true);
        // @ts-ignore
        const spy_provideOne = spyOn(provider, 'provideOne');
        
        provider.provideByName('name');
        
        expect(spy_provideOne).toHaveBeenCalledWith(criteria_always_true);
      });



      it('should return provideOne result', async () => {
        const result_provideOne = valid_providable_01;

        // @ts-ignore
        spyOn(provider, 'createCriteriaToProvideByName').and.returnValue(criteria_always_true);
        // @ts-ignore
        spyOn(provider, 'provideOne').and.returnValue(Promise.resolve(result_provideOne));
        
        const result_provideByName = await provider.provideByName('name');
        
        expect(result_provideByName).toBe(result_provideOne);
      });



      describe('createCriteriaToProvideByName', () => {



        it('should create criteria with the providable\'s name embedded', () => {
          const name = 'name';
  
          const criteria = provider['createCriteriaToProvideByName'](name);
          
          expect(criteria.name).toContain(name);
        });



        it('should throw error if the name is invalid', () => {
          const invalidName = '♣';
          
          spyOn(NameUtil, 'isValid').and.returnValue(false);
          
          expect(() => provider['createCriteriaToProvideByName'](invalidName))
            .toThrowError(Provider.Errors.createCriteriaToProvideByName.invalidParam(invalidName));
        });

      });
      
    });



    it('should return every providable returned by provide', async () => {
      // @ts-ignore
      spyOn(provider, 'provide').and.returnValue(providables);

      const result = await provider.provideMultiple(criteria_always_true);

      expect(result).toBe(providables);
    });
    
  });



  describe('provide', () => {
    let providables: Providable[];
    let provider: Provider<Providable>;

    beforeAll(() => {
      providables = [
        valid_providable_01,
        valid_providable_02,
        valid_providable_03
      ];

      provider = new Provider(providables);
    });

    

    it('should be filted by criteria and limited by max', async () => {
      const doMeetfn = jest.fn();

      const mock_criteria = new Criteria(doMeetfn);
      const max = 2;

      const result = await provider['provide'](mock_criteria, max);

      providables.forEach(p => expect(doMeetfn).toHaveBeenCalledWith(p));
      expect(result.length).toBeLessThanOrEqual(max);
    });


  });



  describe('criteria.filter(this.providables, max)', () => {
    let providables: Providable[];
    let provider: Provider<Providable>;

    beforeAll(() => {
      providables = [
        valid_providable_01,
        valid_providable_02,
        valid_providable_03
      ];

      provider = new Provider(providables);
    });
    


    it('should return every providable if criteria always return true', () => {
      const result = criteria_always_true.filter(provider['providables']);

      expect(result).toEqual(providables);
    });
    


    it('should return an empty array if criteria always return false', () => {
      const result = criteria_always_false.filter(provider['providables']);

      expect(result).toEqual([]);
    });
    


    it('should be limited by max parameter', () => {
      const result = criteria_always_true.filter(provider['providables'], 2);

      expect(result).toEqual([ valid_providable_01, valid_providable_02 ]);
    });
    


    it('should be limited by max parameter but only when the criteria returns true', () => {
      const providables2 = [
        valid_providable_01,
        valid_providable_02,
        valid_providable_03,
        valid_providable_01,
        valid_providable_02,
        valid_providable_03
      ];

      const provider = new Provider(providables2);

      let counter = 0;

      const criteria = new Criteria(
        () => !!(++counter % 2)
      );

      const result = criteria.filter(provider['providables'], 3);

      expect(result).toEqual([ valid_providable_01, valid_providable_03, valid_providable_02 ]);
    });
    
  });



  describe('initProvidables', () => {
    


    it('should call filterProvidablesWithInit method passing providables', async () => {
      const provider = new Provider([]);
      const providables = [
        valid_providable_01,
        valid_providable_02,
        valid_providable_03
      ];

      // @ts-ignore
      const spy_filterProvidablesWithInit = spyOn(provider, 'filterProvidablesWithInit').and.returnValue([]);

      // @ts-ignore
      await provider['initProvidables'](providables);

      expect(spy_filterProvidablesWithInit).toHaveBeenCalledWith(providables);
    });



    it('should wait for every init functions to complete before resolve', async () => {
      const providables = [
        valid_providable_withInit_01,
        valid_providable_withInit_02,
        valid_providable_withInit_03
      ];
      let readyCounter = 0;

      spyOn(valid_providable_withInit_01, 'init').and.callFake(async () => { readyCounter++; return true; })
      spyOn(valid_providable_withInit_02, 'init').and.callFake(async () => { readyCounter++; return true; })
      spyOn(valid_providable_withInit_03, 'init').and.callFake(async () => { readyCounter++; return true; })

      const provider = new Provider<Providable>([]);

      await provider['initProvidables'](providables);

      expect(readyCounter).toBe(providables.length);
    });
    
  });



  describe('filterProvidablesWithInit', () => {
    


    it('should return an empty array if no providable was passed or it is falsy', () => {
      const provider = new Provider([]);

      // @ts-ignore
      const result = provider['filterProvidablesWithInit']();

      expect(result).toEqual([]);
    });



    it('should filter providable with init method', () => {
      const providables = [
        valid_providable_01,
        valid_providable_withInit_01,
        valid_providable_02,
        valid_providable_03,
        valid_providable_withInit_03,
      ];

      const expected = [
        valid_providable_withInit_01,
        valid_providable_withInit_03
      ]

      const provider = new Provider(providables);

      const withInit = provider['filterProvidablesWithInit'](providables);

      expect(withInit).toEqual(expected);
    });



    it('should return an empty array if there is no ProvidableWithInit', () => {
      const providables = [
        valid_providable_01,
        valid_providable_02,
        valid_providable_03
      ];

      const provider = new Provider(providables);

      const withInit = provider['filterProvidablesWithInit'](providables);

      expect(withInit).toEqual([]);
    });
    
  });
  
});
