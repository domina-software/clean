import { Criteria } from '../criteria/criteria';
import { ProvidableUtil, Providable, ProvidableWithInit } from '../providable';
import { NameUtil } from '../forHuman/nameUtil/name.util';



export class Provider<PROVIDABLE extends Providable> {



  constructor(
    private providables: PROVIDABLE[]
  ) {

  }



  public async provideMultiple(criteria: Criteria<PROVIDABLE>): Promise<PROVIDABLE[]> {
    const provided = await this.provide(criteria);

    return provided;
  }



  public async provideOne(criteria: Criteria<PROVIDABLE>): Promise<PROVIDABLE> {
    const provided = (await this.provide(criteria, 1))[0];

    return provided;
  }



  public async provideByName(name: string): Promise<PROVIDABLE> {
    const criteria = this.createCriteriaToProvideByName(name);

    const provided = this.provideOne(criteria);

    return provided;
  }



  protected createCriteriaToProvideByName(name: string): Criteria<PROVIDABLE> {
    if (!NameUtil.isValid(name)) throw Error(Provider.Errors.createCriteriaToProvideByName.invalidParam(name));

    const criteria = new Criteria<PROVIDABLE>(
      providable => providable.name === name,
      `named "${ name }"`,
      'get one providable with the given name',
    );

    return criteria;
  }



  protected async provide(criteria: Criteria<PROVIDABLE>, max?: number): Promise<PROVIDABLE[]> {
    const metCriteria = criteria.filter(this.providables, max);

    await this.initProvidables(metCriteria);

    return metCriteria;
  }



  protected async initProvidables(providables: PROVIDABLE[]): Promise<void> {
    const withInit = this.filterProvidablesWithInit(providables);

    if (withInit.length > 0) {
      await Promise.all(withInit.map(p => p.init()));
    }
  }



  protected filterProvidablesWithInit(providables: PROVIDABLE[]): ProvidableWithInit[] {
    const withInit = providables ?
                        providables.filter(p => typeof p['init'] === 'function') as unknown as ProvidableWithInit[] :
                        [];

    return withInit;
  }


  static readonly Errors = {
    createCriteriaToProvideByName: {
      invalidParam: (name: string) => `"${ name }" is not a valid providable name`
    }
  }
  
}
