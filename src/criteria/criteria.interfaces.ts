



export interface CriteriaFn<PARAM> {
  (param: PARAM): boolean;
}
