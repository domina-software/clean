import { System as CleanSystem, System } from '../system';

import { CriteriaFn } from '.';







export class Criteria<PARAM> {
  static System : CleanSystem;



  public system?: CleanSystem;
  



  constructor(
    protected  doMeetFn: (param: PARAM) => boolean,
    public        name?: string | null,
    public description?: string | null,
    protected toString?: (param: PARAM, result: boolean) => string
  ) {
    if (Criteria.System === undefined) System.getSystem().then(systemSingleton => this.system = Criteria.System = systemSingleton);
    else                                                                          this.system = Criteria.System;
  }



  public filter(toBeFiltered: PARAM[], max = toBeFiltered.length): PARAM[] {
    let counter = 0;

    const filtered = toBeFiltered.filter(p => counter < max && !!(this.fn(p) ? ++counter : false));

    return filtered;
  }



  protected fn(param: PARAM): boolean {
    let doMeet:boolean;
    
    try {
      doMeet = !!this.doMeetFn(param);

      if (this.toString) {
        this.system?.tools.console.info(this.toString(param, doMeet));
      }
    } catch (error) {
      this.system?.tools.console.error(error);
      
      return false;
    }

    return doMeet;
  }



  public getFn(): CriteriaFn<PARAM> {
    const fn = this.fn.bind(this);

    return fn;
  }



  static withAttr<OBJ extends { [key: string] : any }>(attrName: string, attrValue?: any): Criteria<OBJ> {
    const criteria = new Criteria<OBJ>(
      obj => typeof attrValue === 'undefined' ? obj.hasOwnProperty(attrName) : (obj[attrName] === attrValue),
      Criteria.Messages.withAttr.criteriaName(attrName, attrValue),
      Criteria.Messages.withAttr.criteriaDesc(attrName, attrValue)
    );

    return criteria;
  }



  static withName<OBJ extends { name: string }>(name: string): Criteria<OBJ> {
    const criteria = Criteria.withAttr<OBJ>('name', name);

    return criteria;
  }



  static Messages = {
    withAttr: {
      criteriaName: (attrName: string, attrValue?: any) => `"${ attrName }" ${ typeof attrValue === 'undefined' ? 'does exist' : `= ${ attrValue }` }`,
      criteriaDesc: (attrName: string, attrValue?: any) => `Does the object have the "${ attrName }" attribute${ typeof attrValue !== 'undefined' ? ` with value "${ attrValue }"` : '' }`
    }
  };  
  
  
}
