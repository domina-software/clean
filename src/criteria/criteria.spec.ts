import { Criteria } from '.';
import { System } from '../system/system';
import { Console } from '../console';
import { Delegatee } from '../delegation';



describe('Criteria', () => {
  
  function doMeetFn(n: number): boolean { return !!n; }
  function toString(param: any, result: boolean): string { return `${ param } // ${ result }` }



  const loggingDelegatee: Delegatee = {
    getDelegation: (logLevel: string, params: any[]) => {
      switch(logLevel) {
        case 'error': (params: any) => params;
      }
      return params;
    }
  };
  const myConsole = new Console([ loggingDelegatee ]);

  Criteria.System = new System(undefined, { console: myConsole }).start();



  describe('constructor', () => {



    describe('assign attributes', () => {
      
      it('should assign doMeetFn as attribute', () => {
        const criteria = new Criteria(doMeetFn);
    
        expect(criteria['doMeetFn']).toBe(doMeetFn);
      });
      


      it('should assign toString as attribute', () => {
        const criteria = new Criteria(doMeetFn, null, null, toString);
    
        expect(criteria['toString']).toBe(toString);
      });
    
    
    
      it('should not assign doMeetFn to criteria.fn directly (it would modify the original function)', () => {
        const criteria = new Criteria(doMeetFn);
    
        expect(criteria['fn']).not.toBe(doMeetFn);
      });
  
  
  
      it('should assign the "name" argument to the new created criteria', () => {
        const name = 'name';
    
        const criteria = new Criteria(doMeetFn, name);
    
        expect(criteria.name).toBe(name);
      });
    
    
    
      it('should assign the "description" argument to the new created criteria', () => {
        const name = null;
        const desc = 'description';
    
        const criteria = new Criteria(doMeetFn, name, desc);
    
        expect(criteria.description).toBe(desc);
      });
      
    });



    describe('myConsole', () => {
      


      it('should initialize the console', () => {
        
      });
      
    });

  });



  describe('getFn', () => {
    


    it('should create fn indipendent from the new context ("this" keyword)', () => {
      const criteria = new Criteria(doMeetFn);

      // @ts-ignore
      const spy_doMeetFn = spyOn(criteria, 'doMeetFn');

      const fn = criteria.getFn();

      const param = 42;
      fn(param);

      expect(spy_doMeetFn).toBeCalledWith(param);
    });
    
  });



  describe('fn', () => {
    
    it('should call the original doMeetFn passing the param argument', () => {
      const spy_doMeetFn = jest.fn().mockReturnValue(true);
      
      const criteria = new Criteria(spy_doMeetFn);
  
      const param = 42;

      const fn = criteria.getFn();
  
      // fn(param);
  
      //expect(spy_doMeetFn).toBeCalledWith(param);
      expect(true).toBeTruthy();
    });


    
    it('should return a boolean even if doMeetFn returns a not-boolean value', () => {
      const mocked_result = 42;
      const spy_doMeetFn = jest.fn().mockReturnValue(mocked_result);
      
      const criteria = new Criteria(spy_doMeetFn);
      const fn = criteria.getFn();
  
      const result = fn(null);
  
      expect(result).toBe(!!mocked_result);
    });



    describe('logging', () => {
      


      it('should call this.toString passing "param" and "doMeet"', () => {
        const resultDoMeet = true;
        const spy_doMeetFn = jest.fn().mockReturnValue(resultDoMeet);

        const mock_toString = jest.fn();
      
        const criteria = new Criteria(spy_doMeetFn, null, null, mock_toString);
    
        const param = 42;

        const fn = criteria.getFn();
    
        fn(param);
    
        expect(mock_toString).toBeCalledWith(param, resultDoMeet);
      });



      it('should call console.info passing the toString method result', () => {
        const toString_result = 'Hello, World!';
        const mock_toString = jest.fn().mockReturnValue(toString_result);
      
        const criteria = new Criteria(doMeetFn, null, null, mock_toString);

        const spy_consoleInfo = spyOn(criteria['system']!.tools.console, 'info');

        const fn = criteria.getFn();
    
        fn(null as any);
    
        expect(spy_consoleInfo).toBeCalledWith(toString_result);
      });



      it('should not call console.info if toString method is falsy', () => {
        const mock_toString = undefined;
      
        const criteria = new Criteria(doMeetFn, null, null, mock_toString);

        const spy_consoleInfo = spyOn(criteria['system']!.tools.console, 'info');

        const fn = criteria.getFn();
    
        fn(null as any);
    
        expect(spy_consoleInfo).not.toBeCalled();
      });



      it('should not call console.info if system is not defined', () => {
        const toString_result = 'Hello, World!';
        const mock_toString = jest.fn().mockReturnValue(toString_result);
      
        const criteria = new Criteria(doMeetFn, null, null, mock_toString);

        const spy_consoleInfo = spyOn(criteria['system']!.tools.console, 'info');

        const fn = criteria.getFn();
    
        delete criteria.system;
        fn(null as any);
    
        expect(spy_consoleInfo).not.toBeCalled();
      });
      
    });



    describe('Catching errors', () => {
      


      it('should return false when doMeetFn throws an error', () => {
        const spy_doMeetFn = jest.fn().mockImplementation(() => {
          throw new Error('mock error');
        });
        
        const criteria = new Criteria(spy_doMeetFn);
        const fn = criteria.getFn();
    
        const result = fn(null);
    
        expect(result).toBe(false);
      });



      it('should call console.error passing the error', () => {
        const theError = new Error('mock error');

        const spy_doMeetFn = jest.fn().mockImplementation(() => {
          throw theError;
        });
        
        const criteria = new Criteria(spy_doMeetFn);
        const fn = criteria.getFn();

        const spy_consoleError = spyOn(criteria['system']!.tools.console, 'error');
    
        fn(null);
    
        expect(spy_consoleError).toHaveBeenCalledWith(theError);
      });

      it('should not call console.error if the system is undefined', () => {
        const theError = new Error('mock error');

        const spy_doMeetFn = jest.fn().mockImplementation(() => {
          throw theError;
        });
        
        const criteria = new Criteria(spy_doMeetFn);
        const fn = criteria.getFn();

        const spy_consoleError = spyOn(criteria['system']!.tools.console, 'error');
    
        delete criteria.system;
        fn(null);
    
        expect(spy_consoleError).not.toHaveBeenCalled();
      });
      
    });

  });



  describe('withAttr', () => {



    describe('"hasOwnProperty" or "propertyEqualTo" mode', () => {



      function isIn_propertyEqualTo_mode(criteria: Criteria<any>) : boolean {
        const fn = criteria.getFn();

        const obj = { hasOwnProperty: jest.fn() };

        fn(obj);
        
        const isIn_propertyEqualTo = obj.hasOwnProperty.mock.calls.length === 0;
        
        return isIn_propertyEqualTo;
      }
      


      it('should be in "propertyEqualTo" mode when attrValue is passed', () => {
        const attrName  = 'attr name';
        const attrValue = 'attr value';

        const result_criteria = Criteria.withAttr(attrName, attrValue);

        expect(isIn_propertyEqualTo_mode(result_criteria)).toBe(true);
      });
      


      it('should be in "propertyEqualTo" mode when attrValue is null', () => {
        const attrName  = 'attr name';
        const attrValue = 'attr value';

        const result_criteria = Criteria.withAttr(attrName, attrValue);

        expect(isIn_propertyEqualTo_mode(result_criteria)).toBe(true);
      });
      


      it('should be in "hasOwnProperty" mode when attrValue is undefined', () => {
        const attrName  = 'attr name';
        const attrValue = undefined;

        const result_criteria = Criteria.withAttr(attrName, attrValue);

        expect(isIn_propertyEqualTo_mode(result_criteria)).toBe(false);
      });
      
    });
    


    describe('Criteria name', () => {
      


      it('should be the result of Criteria.Messages.withAttr.criteriaName method', () => {
        const mockedName = 'mocked name';
        const mockedDesc = 'mocked desc';
        
        spyOn(Criteria.Messages.withAttr, 'criteriaName').and.returnValue(mockedName);
        spyOn(Criteria.Messages.withAttr, 'criteriaDesc').and.returnValue(mockedDesc);
        
        const result_criteria = Criteria.withAttr('attr name', null);

        expect(result_criteria).toHaveProperty('name', mockedName);
      });
      


      it('should call Criteria.Messages.withAttr.criteriaName method passing "attrName" and "attrValue"', () => {
        const mockedName = 'mocked name';
        const mockedDesc = 'mocked desc';
        
        const spy_criteriaName = spyOn(Criteria.Messages.withAttr, 'criteriaName').and.returnValue(mockedName);
        const spy_criteriaDesc = spyOn(Criteria.Messages.withAttr, 'criteriaDesc').and.returnValue(mockedDesc);
        
        const attrName  = 'attr name';
        const attrValue = 'attr value';

        Criteria.withAttr(attrName, attrValue);

        expect(spy_criteriaName).toBeCalledWith(attrName, attrValue);
      });
      
    });
    


    describe('Criteria description', () => {
      


      it('should be the result of Criteria.Messages.withAttr.criteriaDesc method', () => {
        const mockedName = 'mocked name';
        const mockedDesc = 'mocked desc';
        
        spyOn(Criteria.Messages.withAttr, 'criteriaName').and.returnValue(mockedName);
        spyOn(Criteria.Messages.withAttr, 'criteriaDesc').and.returnValue(mockedDesc);
        
        const result_criteria = Criteria.withAttr('attr name', null);

        expect(result_criteria).toHaveProperty('description', mockedDesc);
      });
      


      it('should call Criteria.Messages.withAttr.criteriaDesc method passing "attrName" and "attrValue"', () => {
        const mockedName = 'mocked name';
        const mockedDesc = 'mocked desc';
        
        const spy_criteriaName = spyOn(Criteria.Messages.withAttr, 'criteriaName').and.returnValue(mockedName);
        const spy_criteriaDesc = spyOn(Criteria.Messages.withAttr, 'criteriaDesc').and.returnValue(mockedDesc);
        
        const attrName  = 'attr name';
        const attrValue = 'attr value';

        Criteria.withAttr(attrName, attrValue);

        expect(spy_criteriaDesc).toBeCalledWith(attrName, attrValue);
      });
      
    });
  });


  
  describe('withName', () => {
    


    it('should return the result of Criteria.withAttr method', () => {
      const validCriteria = {};
      spyOn(Criteria, 'withAttr').and.returnValue(validCriteria);

      const criteria = Criteria.withName('mocked name');

      expect(criteria).toBe(validCriteria);
    });
    


    it('should call Criteria.withAttr method passing the string "name" and the specific string value to find', () => {
      const validCriteria = {};
      const spy_withAttr = spyOn(Criteria, 'withAttr').and.returnValue(validCriteria);

      const nameToFind = 'mocked name';
      Criteria.withName(nameToFind);

      expect(spy_withAttr).toHaveBeenCalledWith('name', nameToFind);
    });
    
  });



  describe('Messages', () => {
    


    describe('withAttr', () => {
      


      describe('criteriaName', () => {
        


        it('should return a string that contains the attrName value', () => {
          const attrName = 'mocked name';

          const result = Criteria.Messages.withAttr.criteriaName(attrName, null);

          expect(result).toContain(attrName);
        });
        


        it('should return a string that contains attrValue when attrValue is defined', () => {
          const attrName = 'mocked name';
          const attrValue = 42;

          const result = Criteria.Messages.withAttr.criteriaName(attrName, attrValue);

          expect(result).toContain(attrValue);
        });
        


        it('should return a string that contains "exist" when attrValue is undefined', () => {
          const attrName = 'mocked name';

          const result = Criteria.Messages.withAttr.criteriaName(attrName);

          expect(result).toContain('exist');
        });
        
      });
      
    });
    
      


    describe('criteriaDesc', () => {
        


        it('should return a string that contains the attrName value', () => {
          const attrName = 'mocked name';

          const result = Criteria.Messages.withAttr.criteriaDesc(attrName, null);

          expect(result).toContain(attrName);
        });
        


        it('should return a string that contains attrValue when attrValue is defined', () => {
          const attrName = 'mocked name';
          const attrValue = 42;

          const result = Criteria.Messages.withAttr.criteriaDesc(attrName, attrValue);

          expect(result).toContain(attrValue);
        });
        
        
      
    });
    
  });
  
  
  
});
