import { Card } from './card';
import { CardParam } from './card.interfaces';



describe('Card', () => {
  


  describe('constructor', () => {
    


    it('should assign cardData to the public attribute', () => {
      const cardData: CardParam = { title: 'The Title', content: 'Hello, World!' };

      const card = new Card(cardData);

      expect(card).toHaveProperty('data', cardData);
    });
    
  });
  
});
