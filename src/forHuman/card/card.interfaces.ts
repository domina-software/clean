import { Card } from './card';



export interface CardParam {
  title: string;
  subtitle?: string;

  content: string | { (): string };
}



export interface CardData {
  id: string;
  title: string;
  subtitle: string | null;

  content: string;
}



export interface AsCard {
  asCard(): Card;
}
