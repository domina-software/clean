import { NameUtil } from './name.util';
import { StringUtil } from '../../util/string.util';
import { timingSafeEqual } from 'crypto';



describe('NameUtil', () => {

  const validName = 'Hello_-World 1';
  


  describe('isValid', () => {
    
    
    
    describe('length', () => {
      let old_minLength: number;
      let old_maxLength: number;
      let minLength = 10;
      let maxLength = 20;
  
  
  
      beforeAll(() => {
        old_minLength = NameUtil.minLength;
        old_maxLength = NameUtil.maxLength;
      });
  
      beforeEach(() => {
        NameUtil.minLength = minLength;
        NameUtil.maxLength = maxLength;
      });



      it('should throw an error when min is greater than max', () => {
        const min = 20;
        const max = 10;

        NameUtil.minLength = min;
        NameUtil.maxLength = max;

        const errorMsg = NameUtil.ErrorMsg.getMinAndMax.minGreaterThanMax(min, max);

        expect(() => NameUtil.isValid('')).toThrowError(errorMsg);
      });
      


      it('should return false when length < minLength', () => {
        const name = StringUtil.generate(NameUtil.minLength - 1, NameUtil.validCharacterSets);
  
        const result = NameUtil.isValid(name);
  
        expect(result).toBe(false);
      });
      
  
  
      it('should return false when length > maxLength', () => {
        const name = StringUtil.generate(NameUtil.maxLength + 1, NameUtil.validCharacterSets);
  
        const result = NameUtil.isValid(name);
  
        expect(result).toBe(false);
      });
      
  
  
      it('should return true when length = minLength', () => {
        const name = StringUtil.generate(NameUtil.minLength, NameUtil.validCharacterSets);
  
        const result = NameUtil.isValid(name);
  
        expect(result).toBe(true);
      });
      
  
  
      it('should return true when length = maxLength', () => {
        const name = StringUtil.generate(NameUtil.maxLength, NameUtil.validCharacterSets);
  
        const result = NameUtil.isValid(name);
  
        expect(result).toBe(true);
      });



      afterAll(() => {
        NameUtil.minLength = old_minLength;
        NameUtil.maxLength = old_maxLength;
      });

    });
    


    describe('null, undefined and other types', () => {
      


      it('should return false when null is passed', () => {
        const result = NameUtil.isValid(null as any);

        expect(result).toBe(false);
      });
      


      it('should return false when undefined is passed', () => {
        const result = NameUtil.isValid(undefined as any);

        expect(result).toBe(false);
      });
      


      it('should return false when array is passed', () => {
        const result = NameUtil.isValid([] as any);

        expect(result).toBe(false);
      });
      


      it('should return false when object is passed', () => {
        const result = NameUtil.isValid({} as any);

        expect(result).toBe(false);
      });
      
    });



    describe('validationRegex', () => {
      let old_regex: RegExp;
      let test_regex = /^([a-zA-Z0-9 _-]+)$/;


      beforeAll(() => {
        old_regex = NameUtil.validationRegex;
        NameUtil.validationRegex = test_regex;
      });



      it('should call the regExp test method', () => {
        const spy_test = spyOn(NameUtil.validationRegex, 'test');

        NameUtil.isValid(validName);

        expect(spy_test).toHaveBeenCalledWith(validName);
      });



      it('should return false when validatiuonRegex.test method returns false', () => {
        const spy_test = spyOn(NameUtil.validationRegex, 'test').and.returnValue(false);

        const result = NameUtil.isValid(validName);

        expect(result).toBeFalsy();
      });



      afterAll(() => {
        NameUtil.validationRegex = old_regex;
      });

    });
    
  });



  describe('lengthInRange', () => {
    const min = 5;
    const max = 7;



    it('should return false if name length is less than min', () => {
      const name = 'bobo';

      const result = NameUtil.lengthInRange(name, { min, max });

      expect(result).toBeFalsy();
    });
    


    it('should return true if name length is equal to min', () => {
      const name = 'natee';

      const result = NameUtil.lengthInRange(name, { min, max });

      expect(result).toBeTruthy();
    });
    


    it('should return true if name length is in between min and max', () => {
      const name = 'sixsix';

      const result = NameUtil.lengthInRange(name, { min, max });

      expect(result).toBeTruthy();
    });



    it('should return true if name length is equal to max', () => {
      const name = '1234567';

      const result = NameUtil.lengthInRange(name, { min, max });

      expect(result).toBeTruthy();
    });



    it('should return false if name length is greater than max', () => {
      const name = '12345678';

      const result = NameUtil.lengthInRange(name, { min, max });

      expect(result).toBeFalsy();
    });



    it('should get min and max from NameUtil.getMinAndMaxLength method if not provided', () => {
      const name = '1';

      const minAndMax = { min: name.length,
                          max: name.length };

      const spy_getMinAndMax = spyOn(NameUtil, 'getMinAndMaxLength').and.returnValue(minAndMax);

      const result = NameUtil.lengthInRange(name);

      expect(spy_getMinAndMax).toHaveBeenCalled();
      expect(result).toBeTruthy();
    });
    
  });



  describe('generateValidName', () => {
    


    describe('maxAttempts', () => {
      let original_maxAttempts: number;



      beforeAll(() => {
        original_maxAttempts = NameUtil.generateValidName_maxAttempts;
      });



      it('should throw an error if NameUtil.generateValidName_maxAttempts is negative', () => {
        const length = undefined;
        const max = -1;
        const expectedError = NameUtil.ErrorMsg.generateValidName.negativeMaxAttempts(max);

        NameUtil.generateValidName_maxAttempts = max;

        expect(() => NameUtil.generateValidName(length, max)).toThrowError(expectedError);
      });



      it('should throw error if it iterate over the max attempts', () => {
        const max = 2;
        NameUtil.generateValidName_maxAttempts = max;

        const generated = ['Hello', 'World', '!'];
        spyOn(StringUtil, 'generate').and.returnValues(...generated);
        
        spyOn(NameUtil, 'isValid').and.returnValue(false);

        const expectedError = NameUtil.ErrorMsg.generateValidName.exeededAttempts(generated.slice(0, max));

        expect(() => NameUtil.generateValidName(undefined, max))
        .toThrowError(expectedError);

      });



      afterAll(() => {
        NameUtil.generateValidName_maxAttempts = original_maxAttempts;
      })
      
    });



    it('should finish in one attempt if default configurations are used', () => {
      const spy_isValid = spyOn(NameUtil, 'isValid').and.callThrough();

      NameUtil.generateValidName();
    
      expect(spy_isValid).toHaveBeenCalledTimes(1);
    });
    
  });



  describe('getMinAndMaxLength', () => {
    let original_min: number;
    let original_max: number;



    beforeAll(() => {
      original_min = NameUtil.minLength;
      original_max = NameUtil.maxLength;
    });



    it('should throw an error if min is greater than max', () => {
      const min = 10;
      const max = 5;
      NameUtil.minLength = min;
      NameUtil.maxLength = max;

      const expectedError = NameUtil.ErrorMsg.getMinAndMax.minGreaterThanMax(min, max);

      expect(() => NameUtil.getMinAndMaxLength())
      .toThrowError(expectedError);
    });



    it('should return values from the NameUtil static attributes', () => {
      const result = NameUtil.getMinAndMaxLength();

      const { minLength: min, maxLength: max } = NameUtil;

      expect(result).toEqual({ min, max });
    });



    afterEach(() => {
      NameUtil.minLength = original_min;
      NameUtil.maxLength = original_max;
    });
    
  });
  
});
