import { StringUtil } from '../../util/string.util';
import { NumberUtil } from '../../util/number.util';



export class NameUtil {
  static minLength =   3;
  static maxLength = 100;

  static validationRegex = /^([a-zA-Z0-9 _-]+)$/;
  static validCharacterSets = [
    'abcdefghijklmnopqrstuvwxyz',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    '0123456789',
    '_ -'
  ];


  static isValid(name: string): boolean {
    const minAndMax = NameUtil.getMinAndMaxLength();

    try {  
      const validLength = NameUtil.lengthInRange(name, minAndMax);

      if (!validLength) throw Error(NameUtil.ErrorMsg.isValid.outOfRange(name, minAndMax));
      
      const acceptedChars = NameUtil.validationRegex.test(name);

      const isValid = validLength && acceptedChars;
  
      return isValid;

    } catch (error) {
      return false;
    }
  }



  static lengthInRange(name: string, { min, max } = NameUtil.getMinAndMaxLength()): boolean {
    const validLength = name.length >= min &&
                        name.length <= max;

    return validLength;
  }



  static generateValidName(length?: number, maxAttempts = NameUtil.generateValidName_maxAttempts): string {
    const { min, max } = NameUtil.getMinAndMaxLength();

    const generated: string[] = [];

    if (maxAttempts < 0) throw Error(NameUtil.ErrorMsg.generateValidName.negativeMaxAttempts(maxAttempts)); 

    for (let iA = 0; iA < maxAttempts; iA++) {
      length = length || NumberUtil.randomBetween(min, max);
  
      const result = StringUtil.generate(length, NameUtil.validCharacterSets);

      generated.push(result);

      if (!NameUtil.isValid(result)) continue;

      return result;
    }

    throw Error(NameUtil.ErrorMsg.generateValidName.exeededAttempts(generated));
  }
  static generateValidName_maxAttempts = 10;



  static getMinAndMaxLength(): { min: number, max: number } {
    let { minLength: min,
          maxLength: max } = NameUtil;

    if (min > max) throw Error(NameUtil.ErrorMsg.getMinAndMax.minGreaterThanMax(min, max));

    return { min, max };
  }


  
  static ErrorMsg = {
    isValid: {
      outOfRange: (provided: string, minAndMax: { min: number, max: number }) => `The provided name "${ provided }" has the length out of range (min: ${ minAndMax.min} - max: ${ minAndMax.max })`
    },
    generateValidName: {
      negativeMaxAttempts: (maxAttempts: number) => `maxAttempts (value: ${ maxAttempts }) must be greater than zero`,
      exeededAttempts: (generated: string[]) => `Attempts to generate a valid name exeeded. Generated: ${ JSON.stringify(generated) }`
    },
    getMinAndMax: {
      minGreaterThanMax: (min: number, max: number) => `NameUtil.minLength: ${ min } cannot be greater than NameUtil.maxLength ${ max }`
    }
  }



}
