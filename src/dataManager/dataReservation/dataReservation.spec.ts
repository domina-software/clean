import { DataReservation } from './dataReservation';
import { ETXTBSY } from 'constants';



describe('dataReservation', () => {
  


  describe('constructor', () => {
    


    it('should assign this.reservedByCategory', () => {
      const newDataReservation = new DataReservation();

      expect(newDataReservation).toHaveProperty('reservedByCategory');
    });
    


    it('should create the default category set', () => {
      const newDataReservation = new DataReservation();

      expect(newDataReservation['reservedByCategory'].has(DataReservation.defaultCategoryName)).toBe(true);
    });



    it('should reserve passed data', () => {
      const toBeReserved = [1, 2, 3];

      const newDataReservation = new DataReservation(toBeReserved);

      const category = newDataReservation['reservedByCategory'].get(DataReservation.defaultCategoryName);
      toBeReserved.forEach(r => expect(category?.has(r)));
    });
    
  });



  describe('reserveMultiple', () => {
    


    it('should assign the default categoryName when nothing is passed', () => {
      const newDataReservation = new DataReservation();

      const spy_reserve = spyOn(newDataReservation, 'reserve');

      const values = [ 1, 2 ];
      newDataReservation.reserveMultiple(values);

      values.forEach(v => expect(spy_reserve).toBeCalledWith(v, DataReservation.defaultCategoryName));
    });

  });



  describe('reserve', () => {
    


    it('should assign the default categoryName when nothing is passed', () => {
      const newDataReservation = new DataReservation();

      const value = 1;
      newDataReservation.reserve(value);

      const category = newDataReservation['reservedByCategory'].get(DataReservation.defaultCategoryName);

      expect(category).toContain(value);
    });
    


    it('should create a new category set if a new categoryName is passed', () => {
      const newDataReservation = new DataReservation();

      const value = 1;
      const categoryName = 'new category';

      expect(newDataReservation['reservedByCategory']).not.toContain(categoryName);

      newDataReservation.reserve(value, categoryName);

      const category = newDataReservation['reservedByCategory'].get(categoryName);

      expect(category).toContain(value);
    });
    


    it('should not create a new category set if an existing categoryName is passed', () => {
      const values = [1, 2];
      const categoryName = DataReservation.defaultCategoryName;

      const newDataReservation = new DataReservation(values);

      const toBeAdded = 3;
      newDataReservation.reserve(toBeAdded, categoryName);

      const category = newDataReservation['reservedByCategory'].get(categoryName);

      [...values, toBeAdded].forEach(v => expect(category).toContain(v));
    });
    


    it('should throw an error if an existing data is passed', () => {
      const values = [1, 2];
      const categoryName = DataReservation.defaultCategoryName;

      const newDataReservation = new DataReservation(values);

      const toBeAdded = values[0];
      
      const expectedError = DataReservation.ErrorMsg.reserve.taken(toBeAdded);

      expect(() => newDataReservation.reserve(toBeAdded, categoryName))
      .toThrowError(expectedError);
    });
    
  });



  describe('isReserved', () => {
    


    it('should get the default category if one is not passed', () => {
      const newDataReservation = new DataReservation();

      const spy_setGet = spyOn(newDataReservation['reservedByCategory'], 'get');

      const value = 42;
      const categoryName = DataReservation.defaultCategoryName;
      
      newDataReservation.isReserved(value);

      expect(spy_setGet).toHaveBeenCalledWith(categoryName);
    });
    


    it('should get the category named as categoryName', () => {
      const newDataReservation = new DataReservation();

      const spy_setGet = spyOn(newDataReservation['reservedByCategory'], 'get');

      const value = 42;
      const categoryName = 'category name';
      
      newDataReservation.isReserved(value, categoryName);

      expect(spy_setGet).toHaveBeenCalledWith(categoryName);
    });
    


    it('should return false if there is no category with the provided name', () => {
      const newDataReservation = new DataReservation();

      const value = 42;
      const categoryName = 'category name';
      
      const result = newDataReservation.isReserved(value, categoryName);

      expect(result).toBeFalsy();
    });
    


    it('should check in the existing category for the value', () => {
      const values = [ '😍', '💓' ];
      const categoryName = DataReservation.defaultCategoryName;

      const newDataReservation = new DataReservation(values);

      const category = newDataReservation['reservedByCategory'].get(categoryName)!;

      const spy_setHas = spyOn(category, 'has');


      const value = values[0];
      
      newDataReservation.isReserved(value, categoryName);

      expect(spy_setHas).toHaveBeenCalledWith(value);
    });
    
  });



  describe('createCategory', () => {
    


    it('should throw an error if the category name is not a valid name', () => {
      const newDataReservation = new DataReservation();

      const invalidName = 'OP$';

      const expectedError = DataReservation.ErrorMsg.createCategory.invalidCategoryName(invalidName);

      expect(() => newDataReservation['createCategory'](invalidName))
      .toThrowError(expectedError);
    });



    it('should create and return the new category set', () => {
      const newDataReservation = new DataReservation();

      const categoryName = 'category name';

      const newCategorySet = newDataReservation['createCategory'](categoryName);
      const category = newDataReservation['reservedByCategory'].get(categoryName);

      expect(category).toEqual(newCategorySet);
    });
    
  });



  describe('ErrorMsg', () => {
    


    describe('reserve', () => {
      


      describe('taken', () => {
        


        it('should return a string', () => {
          const data = { hello: ' ,world!' };
          const theString = DataReservation.ErrorMsg.reserve.taken(data);

          expect(typeof theString).toBe('string');
          expect(theString).toContain('reserve');
        });
        
      });
      
    });


    describe('createCategory', () => {
      


      describe('invalidCategoryName', () => {
        const name = 'a name';
        const theString = DataReservation.ErrorMsg.createCategory.invalidCategoryName(name);

        expect(typeof theString).toBe('string');
        expect(theString).toContain('valid');
      });
      
    });
    
  });
  
});
