import { NameUtil } from '../../forHuman/nameUtil/name.util';



export class DataReservation<DATA_TYPE> {
  static defaultCategoryName: string | symbol = Symbol('noCategory');



  protected reservedByCategory: Map<string | symbol, Set<DATA_TYPE>>;



  constructor(toBeReserved?: DATA_TYPE[]) {
    this.reservedByCategory = new Map();
    this.createCategory(DataReservation.defaultCategoryName);

    if (toBeReserved) this.reserveMultiple(toBeReserved, DataReservation.defaultCategoryName);
  }



  public reserveMultiple(entries: DATA_TYPE[], categoryName = DataReservation.defaultCategoryName) {
    entries.forEach(d => this.reserve(d, categoryName));
  }



  public reserve(data: DATA_TYPE, categoryName = DataReservation.defaultCategoryName) {
    let category = this.reservedByCategory.get(categoryName);

    if (category === undefined) category = this.createCategory(categoryName);
    else if (category.has(data)) throw Error(DataReservation.ErrorMsg.reserve.taken(data));

    category.add(data);
  }



  public isReserved(data: DATA_TYPE, categoryName = DataReservation.defaultCategoryName): boolean {
    const category = this.reservedByCategory.get(categoryName);
    const reserved = !!category?.has(data);

    return reserved;
  }



  protected createCategory(categoryName: string | symbol): Set<DATA_TYPE> {
    if (typeof categoryName === 'string' && !NameUtil.isValid(categoryName)) {
      throw Error(DataReservation.ErrorMsg.createCategory.invalidCategoryName(categoryName));
    }
    
    const newSet = new Set<DATA_TYPE>();
    
    this.reservedByCategory.set(categoryName, newSet);

    return newSet;
  }



  static ErrorMsg = {
    reserve: {
      taken: (data: any) => `"${ JSON.stringify(data) }" is already reserved`
    },
    createCategory: {
      invalidCategoryName: (name: string) => `"${ name }" is not a valid category name`
    }
  }
  
}
