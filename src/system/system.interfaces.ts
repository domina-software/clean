import { System } from './system';
import { FactoryOf } from '../factory/factory.interfaces';



export interface WithStaticSystem {
  System: System;
}



export interface SystemParam {
  classesToAutomaticallyAssignTheSingleton?: (WithStaticSystem | FactoryOf<WithStaticSystem>)[];
}



export interface FullSystemParam {
  classesToAutomaticallyAssignTheSingleton: (WithStaticSystem | FactoryOf<WithStaticSystem>)[];
}



export interface SystemConfig {
  classesToAutomaticallyAssignTheSingleton: WithStaticSystem[];
}


