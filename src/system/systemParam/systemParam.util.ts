import { SystemParam } from '..';
import { FullSystemParam } from '../system.interfaces';



export class SystemParamUtil {



  static mandatoryParamsProperties = [
    'classesToAutomaticallyAssignTheSingleton'
  ];



  static combineSystemParamSets(defaultParam: SystemParam, providedParam?: SystemParam): FullSystemParam {
    const maybeFullSystemParam: SystemParam = {
      ...defaultParam,
      ...providedParam
    };

    const missingSystemParam = SystemParamUtil.missingSystemParamAttrNames(maybeFullSystemParam);
    if (  missingSystemParam.length > 0) throw Error(SystemParamUtil.ErrorMsg.createConfig.isNotFullSystemParam(missingSystemParam));

    return maybeFullSystemParam as FullSystemParam;
  }



  static missingSystemParamAttrNames(systemParam: SystemParam): string[] {
    const missing = SystemParamUtil.mandatoryParamsProperties.filter(a => !systemParam.hasOwnProperty(a));

    return missing;
  }



  static ErrorMsg = {
    createConfig: {
      isNotFullSystemParam: (missing: string[]) => `SystemParam miss ${ JSON.stringify(missing) } parameters`
    }
  }

  
  
}
