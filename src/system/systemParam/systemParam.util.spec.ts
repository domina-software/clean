import { SystemParamUtil } from './systemParam.util';
import { FullSystemParam } from '../system.interfaces';
import { Console } from '../../console';



describe('SystemParamUtil', () => {



  const validDefaultSystemParam: FullSystemParam = {
    classesToAutomaticallyAssignTheSingleton: []
  }



  describe('combineSystemParamSets', () => {
    


    it('should call missingSystemParamAttrNames passing "maybeFullSystemParam"', () => {
      const spy_missingParam = spyOn(SystemParamUtil, 'missingSystemParamAttrNames').and.returnValue([]);

      SystemParamUtil.combineSystemParamSets(validDefaultSystemParam, {});

      expect(spy_missingParam).toBeCalledWith(validDefaultSystemParam);
    });
    


    it('should throw Error when missingSystemParam isn\'t empty', () => {
      const missing = [ 'Ops' ];
      spyOn(SystemParamUtil, 'missingSystemParamAttrNames').and.returnValue(missing);

      const errorMsg = SystemParamUtil.ErrorMsg.createConfig.isNotFullSystemParam(missing);
      
      expect(() => SystemParamUtil.combineSystemParamSets(validDefaultSystemParam, {})).toThrowError(errorMsg);
    });

    
  });



  describe('missingSystemParamAttrNames', () => {
    


    it('should return an empty array if every tool are present', () => {
      const completeParams: any = {};
      SystemParamUtil.mandatoryParamsProperties.forEach(paramName => completeParams[paramName] = {} );

      const result = SystemParamUtil.missingSystemParamAttrNames(completeParams);

      expect(result).toEqual([]);
    });
    


    it('should return every mandatory tools name if none is present', () => {
      const result = SystemParamUtil.missingSystemParamAttrNames({});

      expect(result).toEqual(SystemParamUtil.mandatoryParamsProperties);
    });

  });



  describe('Errors', () => {
    


    describe('createConfig', () => {
      


      describe('isNotFullSystemParam', () => {
        


        it('should return a string containing the missing systemParams', () => {
          const missing = [ 'One', 'Two', 'Three' ];

          const errorMsg = SystemParamUtil.ErrorMsg.createConfig.isNotFullSystemParam(missing);

          missing.forEach(m => expect(errorMsg).toContain(m))
        });
        


        it('should return a string to be like expected (strict equal)', () => {
          const missing = [ 'One', 'Two', 'Three' ];

          const errorMsg = SystemParamUtil.ErrorMsg.createConfig.isNotFullSystemParam(missing);

          expect(errorMsg).toBe('SystemParam miss ["One","Two","Three"] parameters');
        });
        
      });
      
      
    });

  });
  
});
