import { SystemParamUtil } from './systemParam/systemParam.util';
import { SystemConfig, FullSystemParam, WithStaticSystem, SystemParam } from './system.interfaces';
import { SystemToolSet, SystemToolUtil, SystemToolSetWithFactories } from '../systemTool';
import { Criteria } from '../criteria';
import { Console } from '../console';
import { FactoryOf } from '../factory';



export class System {
  static singleton: System;
  static singletonRequests: { (system: System): any }[] = [];
  static defaultSystemParam: FullSystemParam = {
    classesToAutomaticallyAssignTheSingleton: [ () => Criteria ]
  };
  static defaultToolSet: Partial<SystemToolSetWithFactories> = {
    console: () => new Console()
  };


  
  protected fullSystemParam?: FullSystemParam;

  public config!: SystemConfig;

  public  tools!: SystemToolSet;



  constructor(protected providedParam?: SystemParam, protected providedToolSet?: SystemToolSetWithFactories) {
    if (System.singleton) {
      throw Error('System is a singleton and it is already initialized');
    }

  }



  public start(): System {
    this.fullSystemParam     = SystemParamUtil.combineSystemParamSets( System.defaultSystemParam, this.providedParam   );
    const withSomeFactories  =  SystemToolUtil.combineToolSets       ( System.defaultToolSet,     this.providedToolSet );

    const producedTools = SystemToolUtil.productToolFromFactories(withSomeFactories);
    
    const fullToolsetWithoutFactories = {
      ...withSomeFactories,
      ...producedTools
    } as SystemToolSet;

    this.tools = fullToolsetWithoutFactories;

    this.assignSingleton(this, this.fullSystemParam.classesToAutomaticallyAssignTheSingleton);

    return this;
  }



  protected assignSingleton(singleton: System, classesToAutomaticallyAssignTheSingleton?: (WithStaticSystem | FactoryOf<WithStaticSystem>)[]) {
    System.singleton = singleton;

    classesToAutomaticallyAssignTheSingleton?.forEach(c => {
      const theClass = typeof c === 'function' ? c() : c;
      theClass.System = singleton;
    });
    System.singletonRequests.forEach(w => w(singleton));
  }



  static getSystem(): Promise<System> {

    const promise = new Promise<System>((resolve) => {

      System.singleton ?
        resolve(System.singleton) :
        System.singletonRequests.push(resolve);

    });

    return promise;
  }


  
}
