import { System } from './system';
import { SystemParamUtil } from './systemParam/systemParam.util';
import { SystemToolUtil } from '../systemTool';



describe('System', () => {
  

  // TODO: teardown static properties after tests
  describe('constructor', () => {
    


    it('should throw an error when the singleton is already defined', () => {
      delete System.singleton;

      expect(() => new System(null as any)).not.toThrowError();
      
      System['singleton'] = {} as System;

      expect(() => new System(null as any)).toThrowError();
    });



    afterAll(() => {
      delete System.singleton;
    });

    
  });



  describe('start', () => {
    


    it('should assign this.fullSystemParam', () => {
      const system = new System(null as any);

      const fullParam = {};
      const tools = {};

      spyOn(SystemParamUtil, 'combineSystemParamSets').and.returnValue(fullParam);
      spyOn( SystemToolUtil, 'combineToolSets'       ).and.returnValue(tools);

      expect(system).not.toHaveProperty('fullSystemParam');

      system.start();

      expect(system).toHaveProperty('fullSystemParam', fullParam);
    });
    


    it('should assign this.tools', () => {
      const system = new System(null as any);

      const tools = {};
      const fullParam = { tools };

      spyOn(SystemParamUtil, 'combineSystemParamSets').and.returnValue(fullParam);
      spyOn( SystemToolUtil, 'combineToolSets'       ).and.returnValue(tools);

      expect(system).not.toHaveProperty('tools');

      system.start();

      expect(system).toHaveProperty('tools', tools);
    });


    
  });


  afterEach(() => {
    delete System.singleton;
  })
  
});
