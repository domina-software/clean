import { ProvidableUtil } from './providable.util';
import { CardParam, Card } from '../forHuman/card';
import { Providable } from './providable.interfaces';



describe('ProvidableUtil', () => {
  


  describe('asCard', () => {
    


    it('should instantiate a new Card with params created by ProvidableUtil.cardParamOf method', () => {
      const cardParam: CardParam = {
        title: 'title',
        subtitle: 'subtitle',
        content: () => `Hello, World!`
      };
      const spy_cardParamOf = spyOn(ProvidableUtil, 'cardParamOf').and.returnValue(cardParam);

      const providable = { lotOf: 'pretending' } as unknown as Providable;

      const card = ProvidableUtil.asCard(providable);

      expect(card).toHaveProperty('data', cardParam);
      expect(spy_cardParamOf).toHaveBeenCalledWith(providable);
    });
    
  });



  describe('cardParamOf', () => {
    


    it('should map values correctly', () => {
      const theString = `Hello !`;
      const providable = {
        name: 'name',
        desc: 'desc',
        toString: () => theString
      };

      const cardParam = ProvidableUtil.cardParamOf(providable);

      expect(cardParam).toHaveProperty('title',    providable.name);
      expect(cardParam).toHaveProperty('subtitle', providable.desc);
      // @ts-ignore
      expect(cardParam.content()).toBe(theString);
    });
    
  });
  
});
