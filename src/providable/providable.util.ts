import { Card, CardParam } from '../forHuman/card';
import { Providable } from './providable.interfaces';



export class ProvidableUtil {



  static asCard(providable: Providable): Card {
    const cardParam = ProvidableUtil.cardParamOf(providable);

    const newCard = new Card(cardParam);

    return newCard;
  }


  

  static cardParamOf(providable: Providable): CardParam {
    const cardParam: CardParam = {
      title:         providable.name,
      subtitle:      providable.desc,
      content: () => providable.toString()
    };

    return cardParam;
  }


  
}
