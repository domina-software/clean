


export interface Providable {
  name: string;
  desc: string;

  init?(): Promise<this>;
}



export interface ProvidableWithInit extends Providable {
  
  init(): Promise<this>;

}
