import { StringUtil } from './string.util';



describe('StringUtil', () => {
  


  it('should use the provided sets array', () => {
    const spy_getRandomSet = spyOn(StringUtil, 'getRandomSet').and.callThrough();

    const length = 1;
    const sets = [ 'aeiou', '1234' ];

    StringUtil.generate(length, sets);

    expect(spy_getRandomSet).toHaveBeenCalledWith(sets);
  });
  
});
