


export class StringUtil {



  static generate(length: number, sets = StringUtil.defaultGenerateCharacterSets): string {
    let result = '';

    for (let i = 0; i < length; i++) {
      const  set = StringUtil.getRandomSet(sets);
      const char = StringUtil.getRandomCharFromString(set);
      result += char;
    }

    return result;
  }
  static defaultGenerateCharacterSets = [
    'abcdefghijklmnopqrstuvwxyz',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    '0123456789',
    '{[()]}',
    ' _-'
  ];



  static getRandomCharFromString(theString: string, randomNumber = Math.random()): string {
    const randomIndex = Math.floor(randomNumber * theString.length) % theString.length;
    const char = theString[randomIndex];

    return char;
  }



  static getRandomSet(sets: string[], randomNumber = Math.random()): string {
    const randomIndex = Math.floor(randomNumber * sets.length) % sets.length;
    const set = sets[randomIndex];

    return set;
  }


  
}
