import { ObjectUtil } from './object.util';



describe('ObjectUtil', () => {
  


  describe('fromEntries', () => {
    


    it('should compose the object correctly', () => {
      const entries: [string, any][] = [
        ['a', 1],
        ['b', [ true, false]],
        ['c', { i: 'Hello', j: 'World' }]
      ];
      const expected = {
        a: 1,
        b: [ true, false ],
        c: { i: 'Hello', j: 'World' }
      };

      const result = ObjectUtil.fromEntries(entries);

      expect(result).toEqual(expected);
    });
    
  });
  
});
