import { NumberUtil } from './number.util';



describe('NumberUtil', () => {
  


  describe('randomBetween', () => {
    


    it('should work even if min and max are switched', () => {
      const min = 7;
      const max = 9;

      const result = NumberUtil.randomBetween(max, min); // notice: they are switched

      expect(result).toBeGreaterThanOrEqual(min);
      expect(result).toBeLessThanOrEqual(max);
    });
    


    it('should return the min value is Math.random returns zero', () => {
      const min = 7;
      const max = 9;

      spyOn(Math, 'random').and.returnValue(0);

      const result = NumberUtil.randomBetween(min, max);

      expect(result).toBe(min);
    });
    


    it('should return the max value is Math.random returns 0.99', () => {
      const min = 7;
      const max = 9;

      spyOn(Math, 'random').and.returnValue(0.99);

      const result = NumberUtil.randomBetween(min, max);

      expect(result).toBe(max);
    });
    
  });
  
});
