


export class NumberUtil {



  static randomBetween(min: number, max: number): number {
    if (max < min) {
      const temp = min;
      min = max;
      max = temp;
    }
    const random = min + Math.floor((max - min + 1) * Math.random());

    return random;
  }

}
