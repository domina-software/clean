


export class ArrayUtil {



  static extractElementIfAlone<ARR extends any[]>(provided_array: ARR): ARR | ARR[0] {
    const result = provided_array.length === 1 ? provided_array[0] : provided_array;

    return result;
  }
  
}
