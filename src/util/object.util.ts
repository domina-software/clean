


export class ObjectUtil {



  static fromEntries(entries: [string, any][]): object {
    const newObj: { [name: string]: any } = {};

    entries.reduce((acc, [name, value]) => {
      acc[name] = value;

      return acc;
    }, newObj);

    return newObj;
  }
}
