import { Delegatee, DelegateeUtil } from '../delegation';
import { ArrayUtil } from '../util/array.util';



export class Console {



  static defaultDelegatee: Delegatee = {

    getDelegation(type: string, params: any[]) {
      const nativeConsoleType = Console.toNativeConsoleType(type);

      console[nativeConsoleType](...params);
    }

  }



  static typesEqualToTheNativeType_Log   = [ 'info', 'http', 'silly', 'verbose' ];
  static typesEqualToTheNativeType_Warn  = [ 'warn', 'debug'                    ];
  static typesEqualToTheNativeType_Error = [ 'error'                            ];

  static defaultNativeType: 'log' | 'warn' | 'error' = 'log';



  static toNativeConsoleType(customType: string): 'log' | 'warn' | 'error' {
    const   isLogType = Console.typesEqualToTheNativeType_Log.includes(customType);
    if (isLogType)   return 'log';

    const  isWarnType = Console.typesEqualToTheNativeType_Warn.includes(customType);
    if (isWarnType)  return 'warn';

    const isErrorType = Console.typesEqualToTheNativeType_Error.includes(customType);
    if (isErrorType) return 'error';

    return Console.defaultNativeType;
  }



  constructor(protected delegatees: Delegatee[] = [], protected defaultDelegateeParams?: any) {
    this.delegatees = DelegateeUtil.filterValidOnes(   this.delegatees);
    this.delegatees = this.setDefaultDelegateeIfNeeded(this.delegatees, defaultDelegateeParams);
  }




  protected setDefaultDelegateeIfNeeded(delegatees: Delegatee[], defaultDelegateeParams: any): Delegatee[] {
    try {
      if (!Array.isArray(delegatees) || delegatees.length === 0) throw Error();

      return delegatees;

    } catch (error) {
      const defaultDelegatees = [ Console.defaultDelegatee ];

      return defaultDelegatees;
    }
  }



  error(message: string): string;
  error<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0] {
    const toBeReturned = this.logging('error', params);

    return toBeReturned;
  }



  warn(message: string): string;
  warn<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0] {
    const toBeReturned = this.logging('warn', params);

    return toBeReturned;
  }



  info(message: string): string;
  info<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0];
  info<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0] {
    const toBeReturned = this.logging('info', params);

    return toBeReturned;
  }



  http(message: string): string;
  http<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0];
  http<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0] {
    const toBeReturned = this.logging('http', params);

    return toBeReturned;
  }



  verbose(message: string): string;
  verbose<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0];
  verbose<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0] {
    const toBeReturned = this.logging('verbose', params);

    return toBeReturned;
  }



  debug(message: string): string;
  debug<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0];
  debug<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0] {
    const toBeReturned = this.logging('debug', params);

    return toBeReturned;
  }



  silly(message: string): string;
  silly<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0];
  silly<PARAMS extends any[]>(...params: PARAMS): PARAMS | PARAMS[0] {
    const toBeReturned = this.logging('silly', params);

    return toBeReturned;
  }



  logging<PARAMS extends any[]>(logType: string, params: PARAMS): PARAMS | PARAMS[0] {
    this.delegatees.forEach((d, i) => {
      try {
        d.getDelegation(logType, params);
      } catch (error) {
        // remove the bugged delegatee.
        this.delegatees[i] = DelegateeUtil.placeHolder;

        this.error(Console.ErrorMsg.logging.duringDelegation());
      }
    });

    const toBeReturned = ArrayUtil.extractElementIfAlone(params);

    return toBeReturned;
  }



  static ErrorMsg = {
    logging: {
      duringDelegation: () => `Error during log delegation (logging delegatee is removed)`
    }
  }



}
