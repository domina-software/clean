import { Console } from '.';
import { Delegatee } from '../delegation/delegatee/delegatee.interfaces';
import { DelegateeUtil } from '../delegation/delegatee/delegatee.util';



describe('Console', () => {

  const logLevels = [ ...Console.typesEqualToTheNativeType_Log,
                      ...Console.typesEqualToTheNativeType_Warn,
                      ...Console.typesEqualToTheNativeType_Error
                    ]

  const valid_delegatee: Delegatee = {
    getDelegation(toBeReturned: any) { return toBeReturned; }
  };

  const valid_delegatee_2: Delegatee = {
    getDelegation(toBeReturned: any) { return toBeReturned; }
  };

  const invalid_delegatee = {};

  const delegatee_that_throws_error: Delegatee = {
    getDelegation() { throw Error('Ops') }
  };
  


  describe('constructor', () => {
    


    describe('assign attributes', () => {
      

      it('should assign delegatees', () => {
        const myConsole = new Console([ valid_delegatee ]);
        
        expect(myConsole['delegatees']).toEqual([ valid_delegatee ]);
      });



      it('should set the default delegatee if none is provided', () => {
        const delegatees: Delegatee[] = [];

        const myConsole = new Console(delegatees);
        
        expect(myConsole['delegatees']).toEqual([ Console.defaultDelegatee ]); 
      });



      it('should set the default delegatee when passing null', () => {
        const myConsole = new Console(null as any);
        
        expect(myConsole['delegatees']).toEqual([ Console.defaultDelegatee ]); 
      });



      it('should filter out invalid delegatee', () => {
        const myConsole = new Console([
                                  valid_delegatee,
                                invalid_delegatee as any,
                                  valid_delegatee_2,
                                  null,
                                  42 
        ]);
        
        expect(myConsole['delegatees']).toEqual([
                                          valid_delegatee,
                                          valid_delegatee_2
        ]); 
      });



      it('should assign defaultDelegateeParams to the attribute', () => {
        const defaultDelegateeParams = {};

        const myConsole = new Console([ valid_delegatee ], defaultDelegateeParams);
        
        expect(myConsole['defaultDelegateeParams']).toEqual(defaultDelegateeParams);
      });
      
    });
    
  });



  describe('setDefaultDelegateeIfNeeded', () => {
    


    it('should do nothing if an array with lenght greater than 0 is passed', () => {
      const delegatees = [ valid_delegatee ];

      const myConsole = new Console(delegatees);

      expect(myConsole['delegatees']).toEqual(delegatees);
    });
    


    it('should do nothing if an array with lenght greater than 0 is passed', () => {
      const delegatees = [ valid_delegatee ];

      const myConsole = new Console(delegatees);

      expect(myConsole['delegatees']).toEqual(delegatees);
    });



    it('should return [ Console.defaultDelegatee ] when there is no valid delegatee', () => {
      const delegatees = [ null ] as unknown as Delegatee[];

      const myConsole = new Console(delegatees);

      expect(myConsole['delegatees']).toEqual([ Console.defaultDelegatee ]);
    });
    
  });



  describe('info', () => {
    


    it('should call logging method passing type and parameters', () => {
      const delegatees = [ valid_delegatee ];

      const myConsole = new Console(delegatees);
      // @ts-ignore
      const spy_logging = spyOn(myConsole, 'logging');

      const message = 'logging message';

      myConsole.info(message);

      expect(spy_logging).toBeCalledWith('info', [ message ]);
    });
    


    it('should return the provided parameter', () => {
      const delegatees = [ valid_delegatee ];

      const myConsole = new Console(delegatees);

      const message = 'logging message';

      const result = myConsole.info(message);

      expect(result).toBe(message);
    });
    


    it('should return the provided parameters as an array', () => {
      const delegatees = [ valid_delegatee ];

      const myConsole = new Console(delegatees);

      const param_1 = 'logging message';
      const param_2 = 42;

      const result = myConsole.info(param_1, param_2);

      expect(result).toEqual([ param_1, param_2 ]);
    });



    it('should be called from every level logging methods', () => {
      const delegatees = [ valid_delegatee ];

      const myConsole = new Console(delegatees);

      const spy_logging = spyOn(myConsole, 'logging');

      // @ts-ignore
      logLevels.forEach(l => myConsole[l](l));

      expect(spy_logging).toBeCalledTimes(logLevels.length);
      
      logLevels.forEach((l, i) =>
        expect(spy_logging.calls.all()[i].args).toEqual([l, [l]])
      );
    });
    
  });



  describe('logging', () => {
    


    it('should call every delegatee passing logType and params', () => {
      const delegatees = [ valid_delegatee, valid_delegatee_2 ];

      const myConsole = new Console(delegatees);

      const spy_delegatee_1 = spyOn(myConsole['delegatees'][0], 'getDelegation');
      const spy_delegatee_2 = spyOn(myConsole['delegatees'][1], 'getDelegation');

      const param_1 = 'logging message';
      const param_2 = 42;

      myConsole.info(param_1, param_2);

      expect(spy_delegatee_1).toBeCalledWith('info', [ param_1, param_2 ]);
      expect(spy_delegatee_2).toBeCalledWith('info', [ param_1, param_2 ]);
    });



    it('should remove delegatee that throws an error', () => {
      const delegatees = [ delegatee_that_throws_error, valid_delegatee_2 ];

      const myConsole = new Console(delegatees);

      const param_1 = 'logging message';
      const param_2 = 42;

      myConsole.info(param_1, param_2);

      expect(myConsole['delegatees'][0]).toBe(DelegateeUtil.placeHolder);
    });
    
  });



  describe('defaultDelegatee', () => {
    const myConsole = new Console();
    const message = 'Hello, world!';
    


    it('should call the native "log" console method', () => {
      const spy_console = spyOn(console, 'log');

      myConsole.info(message);

      expect(spy_console).toBeCalledWith(message);
    });
    


    it('should call the native "warn" console method', () => {
      const spy_console = spyOn(console, 'warn');

      myConsole.warn(message);

      expect(spy_console).toBeCalledWith(message);
    });
    


    it('should call the native "error" console method', () => {
      const spy_console = spyOn(console, 'error');

      myConsole.error(message);

      expect(spy_console).toBeCalledWith(message);
    });

  });



  describe('toNativeConsoleType', () => {
  
    it('should return "log" is elements of typesEqualToTheNativeType_Log are passed', () => {
      const isAllTrue = Console.typesEqualToTheNativeType_Log.every(t => Console.toNativeConsoleType(t) === 'log');

      expect(isAllTrue).toBe(true);
    });
  


    it('should return "warn" is elements of typesEqualToTheNativeType_Warn are passed', () => {
      const isAllTrue = Console.typesEqualToTheNativeType_Warn.every(t => Console.toNativeConsoleType(t) === 'warn');

      expect(isAllTrue).toBe(true);
    });
  


    it('should return "error" is elements of typesEqualToTheNativeType_Error are passed', () => {
      const isAllTrue = Console.typesEqualToTheNativeType_Error.every(t => Console.toNativeConsoleType(t) === 'error');

      expect(isAllTrue).toBe(true);
    });
  


    it('should return value in Console.defaultNativeType when unknown string is passed', () => {
      const result = Console.toNativeConsoleType('unknown type');

      expect(result).toBe(Console.defaultNativeType);
    });
      
    
  });



  describe('ErrorMsg', () => {
    


    describe('logging', () => {
      


      describe('duringDelegation', () => {
        


        it('should return a not-empty string', () => {
          const result = Console.ErrorMsg.logging.duringDelegation();

          expect(typeof result).toBe('string');
          expect(result.length).toBeGreaterThan(0);
        });
        
      });
      
    });
    
  });
  
});
