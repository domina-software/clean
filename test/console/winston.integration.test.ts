import { Console } from '../../src/console';
import { Delegatee } from '../../src/delegation';

import { createLogger, transports } from 'winston';



describe('Console', () => {
  


  describe('winston integration', () => {


    it('should work correctly', () => {
      const winston = createLogger({
        transports: [
          new transports.Console()
        ]
      });
      
      const winstonAdapter: Delegatee = {
        getDelegation(level: string, params: any[]) {
          const message = JSON.stringify(params);

          console.log(`from winston:`, { level, message });

          winston.log({ level, message });
        }
      };
  
      const myConsole = new Console([ winstonAdapter ]);
  
      const spy_getDelegation = spyOn(myConsole['delegatees'][0], 'getDelegation');
  
      myConsole.info('test', 42);
  
      expect(spy_getDelegation).toBeCalledWith('info', [ 'test', 42 ]);
    });

  });
  
});
