import {        Card } from '../src/forHuman/card';
import {      System, SystemParamUtil } from '../src/system';
import {     Console } from '../src/console';
import { DataManager,
     DataReservation } from '../src/dataManager';
import { FactoryUtil } from '../src/factory';
import { ProvidableUtil } from '../src/providable';



describe('clean', () => {
  


  describe('export', () => {
    


    describe('Card', () => {



      it('should import', () => {
        const { Card: required_Card } = require('../src/clean');
        
        expect(required_Card).toBe(Card);
      });
    });
    


    describe('System', () => {



      it('should import', () => {
        const { System: required_System } = require('../src/clean');
        
        expect(required_System).toBe(System);
      });
    });
    


    describe('SystemParamUtil', () => {



      it('should import', () => {
        const { SystemParamUtil: required_SystemParamUtil } = require('../src/clean');
        
        expect(required_SystemParamUtil).toBe(SystemParamUtil);
      });
    });
    


    describe('Console', () => {



      it('should import', () => {
        const { Console: required_Console } = require('../src/clean');
        
        expect(required_Console).toBe(Console);
      });
    });
    


    describe('Criteria', () => {



      it('should import', () => {
        const { Criteria: required_Criteria } = require('../src/clean');
        
        const criteria = new required_Criteria(() => true);

        expect(criteria).toHaveProperty('getFn');
      });
    });


    


    describe('DataManager', () => {



      it('should import', () => {
        const { DataManager: required_DataManager } = require('../src/clean');
        
        expect(required_DataManager).toBe(DataManager);
      });
    });

    


    describe('DataReservation', () => {



      it('should import', () => {
        const { DataReservation: required_DataReservation } = require('../src/clean');
        
        expect(required_DataReservation).toBe(DataReservation);
      });
    });
    


    describe('FactoryUtil', () => {



      it('should import', () => {
        const { FactoryUtil: required_FactoryUtil } = require('../src/clean');
        
        expect(required_FactoryUtil).toBe(FactoryUtil);
      });
    });
    


    describe('ProvidableUtil', () => {



      it('should import', () => {
        const { ProvidableUtil: required_ProvidableUtil } = require('../src/clean');
        
        expect(required_ProvidableUtil).toBe(ProvidableUtil);
      });
    });
    
  });
  
});
